ARG ARCH=arm64v8
ARG PY_VERSION=3.7
FROM ${ARCH}/python:${PY_VERSION}-slim

ARG PY_VERSION=3.7
ARG CMAKE_VERSION=3.16.5
ARG OPENCV_VERSION=4.5.0
ARG ONNXRUNTIME_VERSION=1.8.1
ARG PYTORCH_VERSION=1.7.0
ARG TORCHVISION_VERSION=0.8.1
ARG NUM_JOBS=12

# ----------------------------------------------------------------------------------------------------------------------
# Install libraries
# ----------------------------------------------------------------------------------------------------------------------
RUN apt-get update && apt-get -y --no-install-recommends install \
    build-essential \
    libcurl4-openssl-dev \
    zlib1g-dev \
    libssl-dev \
    libgtk-3-dev \
    libjpeg-dev \
    libpng-dev \
    libcanberra-gtk-module \
    libsm6 \
    libxext6 \
    libxrender-dev \
    libglib2.0-0 \
    libgtk2.0-0 \
    libxtst6 \
    libxss1 \
    libx11-6 \
    libgconf-2-4 \
    libnss3 \
    libasound2 \
    libgstreamer1.0-0 \
    libgstreamer1.0-dev \
    libgstreamer-plugins-base1.0-dev \
    libprotobuf-dev \
    libavcodec-dev \
    libavformat-dev \
    libswscale-dev \
    libopenexr-dev \
    libtiff-dev \
    libturbojpeg-dev \
    libwebp-dev \
    libglib2.0-doc \
    python3-dev
# ----------------------------------------------------------------------------------------------------------------------

# ----------------------------------------------------------------------------------------------------------------------
# Install utility software
# ----------------------------------------------------------------------------------------------------------------------
RUN apt-get update && apt-get -y install \
    autoconf \
    automake \
    libtool \
    pkg-config \
    openssl \
    software-properties-common \
    gnupg  \
    apt-transport-https \
    ca-certificates \
    sudo \
    git \
    curl \
    unzip \
    bzip2 \
    wget \
    xvfb \
    xauth \
    protobuf-compiler \
    ffmpeg \
    gstreamer1.0-tools \
    gstreamer1.0-plugins-base \
    gstreamer1.0-plugins-good \
    gstreamer1.0-plugins-bad \
    gstreamer1.0-plugins-ugly \
    gstreamer1.0-libav \
    gstreamer1.0-x \
    gstreamer1.0-alsa \
    gstreamer1.0-gl \
    gstreamer1.0-gtk3 \
    gstreamer1.0-pulseaudio \
    locales \
    locales-all
# ----------------------------------------------------------------------------------------------------------------------


RUN apt-get clean

# ----------------------------------------------------------------------------------------------------------------------
# Install cmake
# ----------------------------------------------------------------------------------------------------------------------
RUN cd /tmp && wget https://github.com/Kitware/CMake/releases/download/v${CMAKE_VERSION}/cmake-${CMAKE_VERSION}.tar.gz && \
    tar -xvf cmake-${CMAKE_VERSION}.tar.gz && \
    cd cmake-${CMAKE_VERSION} && \
    ./bootstrap --system-curl && \
    make && \
    make install && \
    rm -rf /tmp/*
# ----------------------------------------------------------------------------------------------------------------------

# ----------------------------------------------------------------------------------------------------------------------
# Python packages
# ----------------------------------------------------------------------------------------------------------------------
RUN python -m pip install --user --upgrade pip && \
    python -m pip install --user wheel setuptools \
        numpy==1.19.4 \
        Cython==0.29.23
# ----------------------------------------------------------------------------------------------------------------------

# ----------------------------------------------------------------------------------------------------------------------
# Install OpenCV
# ----------------------------------------------------------------------------------------------------------------------
RUN cd /tmp && \
    wget -O opencv.zip https://github.com/opencv/opencv/archive/${OPENCV_VERSION}.zip && \
    wget -O opencv_contrib.zip https://github.com/opencv/opencv_contrib/archive/${OPENCV_VERSION}.zip && \
    wget -O opencv_extra.zip https://github.com/opencv/opencv_extra/archive/${OPENCV_VERSION}.zip && \
    unzip opencv.zip && \
    unzip opencv_contrib.zip && \
    unzip opencv_extra.zip && \
    mkdir -p build && cd build && \
    cmake -DOPENCV_EXTRA_MODULES_PATH=../opencv_contrib-${OPENCV_VERSION}/modules \
        -D CMAKE_USE_OPENSSL=ON \
        -DCMAKE_BUILD_TYPE=Release \
        -DCMAKE_INSTALL_PREFIX=/usr \
        -DBUILD_PNG=OFF \
        -DBUILD_TIFF=OFF \
        -DBUILD_TBB=OFF \
        -DBUILD_JPEG=OFF \
        -DBUILD_JASPER=OFF \
        -DBUILD_ZLIB=OFF \
        -DBUILD_EXAMPLES=ON \
        -DBUILD_JAVA=OFF \
        -DBUILD_opencv_python2=OFF \
        -DBUILD_opencv_python3=ON \
        -DCMAKE_INSTALL_PREFIX=$(python${PY_VERSION} -c "import sys; print(sys.prefix)") \
        -DPYTHON3_EXECUTABLE=$(which python${PY_VERSION}) \
        -DPYTHON3_INCLUDE_DIR=$(python${PY_VERSION} -c "from distutils.sysconfig import get_python_inc; print(get_python_inc())") \
        -DPYTHON3_PACKAGES_PATH=$(python${PY_VERSION} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())") \
        -DWITH_OPENCL=OFF \
        -DWITH_OPENMP=OFF \
        -DWITH_FFMPEG=ON \
        -DWITH_GSTREAMER=ON \
        -DWITH_GSTREAMER_0_10=OFF \
        -DWITH_GTK=ON \
        -DWITH_VTK=OFF \
        -DWITH_TBB=ON \
        -DWITH_1394=OFF \
        -DWITH_OPENEXR=OFF \
        -DINSTALL_C_EXAMPLES=OFF \
        -DINSTALL_TESTS=OFF \
        -DOPENCV_TEST_DATA_PATH=../opencv_extra-${OPENCV_VERSION}/testdata \
        ../opencv-${OPENCV_VERSION} && \
    sudo cmake --build . --parallel ${NUM_JOBS} && \
    sudo make install && \
    sudo ldconfig && \
    sudo rm -rf /tmp/*
# ----------------------------------------------------------------------------------------------------------------------

# ----------------------------------------------------------------------------------------------------------------------
# Install Onnxruntime
# ----------------------------------------------------------------------------------------------------------------------
RUN python -m pip install --user onnxruntime==${ONNXRUNTIME_VERSION}
# ----------------------------------------------------------------------------------------------------------------------

# ----------------------------------------------------------------------------------------------------------------------
# Install Torch & Torchvision Wheel
# ----------------------------------------------------------------------------------------------------------------------
RUN apt-get install -y python3-pip libopenblas-dev libopenmpi-dev libomp-dev libavcodec-dev libavformat-dev libswscale-dev && \
    python3 -m pip install gdown

RUN gdown https://drive.google.com/uc?id=1o2PjvvKkHpYx7QMfMLUoNck7ZiE_TGgR && \
    python3 -m pip install torch-1.7.0a0-cp37-cp37m-linux_aarch64.whl && \
    rm torch-1.7.0a0-cp37-cp37m-linux_aarch64.whl

RUN gdown https://drive.google.com/uc?id=1H3YLkOicAN78tBLAmzCx2lt_WBjjmFCz && \
    python3 -m pip install torchvision-0.8.0a0+45f960c-cp37-cp37m-linux_aarch64.whl && \
    rm torchvision-0.8.0a0+45f960c-cp37-cp37m-linux_aarch64.whl
# ----------------------------------------------------------------------------------------------------------------------

# ----------------------------------------------------------------------------------------------------------------------
# Install Torch & Torchvision Source
# ----------------------------------------------------------------------------------------------------------------------
#ENV USE_CUDA=0
#ENV USE_CUDNN=0
#ENV NO_DISTRIBUTED=1
#ENV USE_MKLDNN=0
#ENV BUILD_TEST=0
#ENV MAX_JOBS=12
#
#RUN python3 -m pip install pyyaml
#
#RUN cd /tmp && \
#    git clone -b v${PYTORCH_VERSION} --depth=1 --recursive https://github.com/pytorch/pytorch.git && \
#    cd pytorch && \
#    python3 -m pip install -r requirements.txt && \
#    python3 setup.py bdist_wheel && python3 -m pip install dist/* && rm -rf /tmp
#
#RUN mkdir /tmp && cd /tmp && \
#    git clone --recursive https://github.com/pytorch/vision && cd vision && \
#    git checkout v0.8.1 && \
#    python3 setup.py bdist_wheel && python3 -m pip install dist/* && rm -rf /tmp/*
# ----------------------------------------------------------------------------------------------------------------------

# ----------------------------------------------------------------------------------------------------------------------
# Setup AI Asset
# ----------------------------------------------------------------------------------------------------------------------
WORKDIR /app
ADD . /app

# pre install
# If you are using submodule and want to apply a patch, try to use or modify this line
# RUN cd /app/source/Submodule name && git apply /app/source/patch/modification_1.patch
RUN python3 -m pip install --user -e .[rpi_arm64v8]

# post install
RUN cd /app && sudo rm -rf .git && \
    sudo chmod -R +x /app/interface/
# ----------------------------------------------------------------------------------------------------------------------

# Set default command
# ----------------------------------------------------------------------------------------------------------------------
CMD ["/bin/bash"]
# ----------------------------------------------------------------------------------------------------------------------
