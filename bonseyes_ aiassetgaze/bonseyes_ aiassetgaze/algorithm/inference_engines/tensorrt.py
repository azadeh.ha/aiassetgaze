import numpy as np
import tensorrt as trt
import pycuda
import pycuda.driver as cuda
import pycuda.autoinit

from .inference_engine import InferenceEngineBase


def GB(val):
    return val * 1 << 30


class HostDeviceMem:
    def __init__(self, host_mem, device_mem):
        self.host = host_mem
        self.device = device_mem

    def __str__(self):
        return "Host:\n" + str(self.host) + "\nDevice:\n" + str(self.device)

    def __repr__(self):
        return self.__str__()


class TensorrtEngine(InferenceEngineBase):
    def __init__(self, model_path):
        self.batch_size = 1
        self.workspace_size = GB(4)
        self.trt_logger = trt.Logger(trt.Logger.WARNING)
        self.ctx = cuda.Device(0).make_context()
        self.stream = cuda.Stream()

        self.inputs = []
        self.outputs = []
        self.bindings = []
        self.outputs_num = None
        self.trt_engine = self.load(model_path)

    def destroy(self):
        self.ctx.pop()
        del self.trt_engine

    def load_onnx(self, model_path):
        network_creation_flag = 1 << int(trt.NetworkDefinitionCreationFlag.EXPLICIT_PRECISION)
        network_creation_flag |= 1 << int(trt.NetworkDefinitionCreationFlag.EXPLICIT_BATCH)
        with trt.Builder(self.trt_logger) as builder, builder.create_network(
            network_creation_flag
        ) as network, trt.OnnxParser(network, self.trt_logger) as parser:
            builder.max_workspace_size = self.workspace_size
            # Load the Onnx model and parse it in order to populate the TensorRT network.
            with open(model_path, 'rb') as model:
                parser.parse(model.read())
            return builder.build_cuda_engine(network)

    def load_trt(self, model_path):
        self.ctx.push()

        with open(model_path, 'rb') as fin, trt.Runtime(self.trt_logger) as runtime:
            tensorrt_engine = runtime.deserialize_cuda_engine(fin.read())

        self.inputs, self.outputs, self.bindings = self._allocate_buffers(tensorrt_engine, self.batch_size)
        self.outputs_num = len(self.outputs)

        self.ctx.pop()
        return tensorrt_engine

    def load(self, model_path):
        if model_path.split('.')[-1] == 'onnx':
            return self.load_onnx(model_path)
        else:
            return self.load_trt(model_path)

    def infer(self, image):
        """Infer for multiple inputs/outputs.
        Inputs and outputs are expected to be lists of HostDeviceMem objects.
        """
        self.ctx.push()
        norm_img = self._normalize_image(image)
        self.inputs[0].host = norm_img
        with self.trt_engine.create_execution_context() as context:
            # Transfer input data to the GPU
            [cuda.memcpy_htod_async(inp.device, inp.host, self.stream) for inp in self.inputs]
            # Run inference
            context.execute_async(
                batch_size=self.batch_size,
                bindings=self.bindings,
                stream_handle=self.stream.handle,
            )
            # Transfer predictions back from GPU
            [cuda.memcpy_dtoh_async(out.host, out.device, self.stream) for out in self.outputs]
            # Syn with steam
            self.stream.synchronize()

        prediction = self.outputs[0].host

        self.ctx.pop()
        return prediction

    def _normalize_image(self, image):
        image_arr = np.asarray(image).transpose([2, 0, 1]).astype(trt.nptype(trt.float32)).ravel()
        image_arr = (image_arr / 255.0 - 0.45) / 0.225

        return image_arr

    def _allocate_buffers(self, engine, batch_size):
        """Allocates all buffers required for an engine, i.e. host/device inputs/outputs"""
        self.ctx.push()

        inputs = []
        outputs = []
        bindings = []
        for binding in engine:
            size = trt.volume(engine.get_binding_shape(binding)) * batch_size
            dtype = trt.nptype(engine.get_binding_dtype(binding))

            # Allocate host and device buffers
            host_mem = cuda.pagelocked_empty(size, dtype)
            device_mem = cuda.mem_alloc(host_mem.nbytes)

            # Append the device buffer to device bindings.
            bindings.append(int(device_mem))

            # Append to the appropriate list.
            if engine.binding_is_input(binding):
                inputs.append(HostDeviceMem(host_mem, device_mem))
            else:
                outputs.append(HostDeviceMem(host_mem, device_mem))
        self.ctx.pop()

        return inputs, outputs, bindings

