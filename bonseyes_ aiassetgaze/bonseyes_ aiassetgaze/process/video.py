import threading
import argparse
import queue
import time
import json
import cv2
import sys
import os
import pandas as pd
import psutil

from bonseyes_ aiassetgaze.utils.meter import HardwareStatusMeter
from bonseyes_ aiassetgaze.algorithm.algorithm import Algorithm, AlgorithmInput
from bonseyes_ aiassetgaze.utils.gstreamer_pipelines import (
    capture_pipeline as capture_template,
    sink_pipeline as sink_template
)

sys.path.append("/app/source/Submodule name")

BUF_SIZE = 10000
frame_queue = queue.Queue(BUF_SIZE)
finished_flag_queue = queue.Queue(BUF_SIZE)
hardware_status_meter = HardwareStatusMeter(sample_rate_sec=0.05)

class FrameProducerThread(threading.Thread):
    def __init__(self, capture, grayscale):
        super(FrameProducerThread, self).__init__()

        self.capture = capture
        self.grayscale = grayscale

    def run(self):
        while self.capture.isOpened():
            if not frame_queue.full():
                ret, frame = self.capture.read()
                if not ret:
                    break

                if self.grayscale:
                    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
                    frame = cv2.cvtColor(frame, cv2.COLOR_GRAY2BGR)

                frame_queue.put(frame)

        finished_flag_queue.put(True)
        return


class InferenceAndDisplayThread(threading.Thread):
    def __init__(
        self,
        sink,
        algorithm,
        output_json,
        output_csv,
        debug_info,
        attach_logo
    ):
        super(InferenceAndDisplayThread, self).__init__()
        self.sink = sink
        self.algorithm = algorithm
        self.output_json = output_json
        self.output_csv = output_csv
        self.debug_info = debug_info
        self.attach_logo = attach_logo
        self.json_result = {}

        self.post_processing_time = 0
        self.pre_processing_time = 0
        self.infer_time = 0

        self.cpu_usage = 0
        self.gpu_usage = 0

        self.frame_counter = 0
        self.display_time = 0
        self.algorithm_time = 0
        self.display_fps = 0
        self.algorithm_fps = 0

        self.calculate_every = 10
        self.logo = cv2.imread('/app/doc/logo-header.png')

    def _attach_watermark(self, rendered_frame):
        alpha = 0.5
        padding = 5

        sy = rendered_frame.shape[0] - self.logo.shape[0] - padding
        ey = sy + self.logo.shape[0]
        sx = padding
        ex = padding + self.logo.shape[1]

        added_image = cv2.addWeighted(rendered_frame[sy:ey, sx:ex, :], alpha, self.logo, 1 - alpha, 0)
        rendered_frame[sy:ey, sx:ex, :] = added_image

        return rendered_frame

    def _render_frame(self, frame, algorithm_result=None):
        if self.debug_info:
            font_color = (255, 255, 255)

            fps_infer = 'Algorithm FPS: ' + str(round(self.algorithm_fps, 1))
            cv2.putText(frame, fps_infer, (10, 20), cv2.FONT_HERSHEY_DUPLEX, 0.5, font_color, lineType=cv2.LINE_AA)
            fps_display = 'Display FPS: ' + str(round(self.display_fps, 1))
            cv2.putText(frame, fps_display, (10, 40), cv2.FONT_HERSHEY_DUPLEX, 0.5, font_color, lineType=cv2.LINE_AA)

            cpu_usage = 'CPU usage: ' + str(self.cpu_usage) + '%'
            cv2.putText(frame, cpu_usage, (10, 70), cv2.FONT_HERSHEY_DUPLEX, 0.5, font_color, lineType=cv2.LINE_AA)
            gpu_usage = 'GPU usage: %s' % str(self.gpu_usage) + '%'
            cv2.putText(frame, gpu_usage, (10, 90), cv2.FONT_HERSHEY_DUPLEX, 0.5, font_color, lineType=cv2.LINE_AA)

            input_size = 'Input size: %sx%s' % (self.algorithm.input_size, self.algorithm.input_size)
            cv2.putText(frame, input_size, (10, 120), cv2.FONT_HERSHEY_DUPLEX, 0.5, font_color, lineType=cv2.LINE_AA)

            preproc_time = 'Pre processing: ' + str(round(self.pre_processing_time, 1)) + 'ms'
            cv2.putText(frame, preproc_time, (10, 150), cv2.FONT_HERSHEY_DUPLEX, 0.5, font_color, lineType=cv2.LINE_AA)
            forward_pass_time = 'Forward: ' + str(round(self.infer_time, 1)) + 'ms'
            cv2.putText(
                frame, forward_pass_time, (10, 170), cv2.FONT_HERSHEY_DUPLEX, 0.5, font_color, lineType=cv2.LINE_AA
            )
            postproc_time = 'Post processing: ' + str(round(self.post_processing_time, 1)) + 'ms'
            cv2.putText(frame, postproc_time, (10, 190), cv2.FONT_HERSHEY_DUPLEX, 0.5, font_color, lineType=cv2.LINE_AA)

        if algorithm_result:
            rendered_frame = self.algorithm.render(frame, algorithm_result)
        else:
            rendered_frame = frame

        if self.attach_logo:
            rendered_frame = self._attach_watermark(rendered_frame)

        return rendered_frame

    def _save_frame_results(self, frame_id, algorithm_result, additional_stats):
        result = [item.dict for item in algorithm_result]
        self.json_result[frame_id] = {'predictions': result, 'stats': additional_stats}

    def _transform_json_for_csv_conversion(self, json_result):
        result = []
        for frame_id, value in json_result.items():
            predictions = value['predictions']
            stats = value['stats']
            item = dict(
                FRAME_ID=frame_id,
                CPU_USAGE=str(stats['cpu_usage_current']) + '%',
                GPU_USAGE=str(stats['gpu_usage_current']) + '%',
                GPU_MEMORY=str(stats['gpu_mem']) + 'GB',
                PREPROCESSING_TIME=stats['preproc_time_current'],
                INFERENCE_TIME=stats['infer_time_current'],
                POSTPROCESSING_TIME=stats['postproc_time_current'],
                TOTAL_TIME=stats['total_time'],
                PREDICTIONS=predictions,
            )
            result.append(item)

        return json.dumps(result)

    def run(self):
        while True:
            if not frame_queue.empty():
                t_display_s = time.time()
                frame = frame_queue.get()

                t_algorithm_s = time.time()
                result = self.algorithm.process(frame)
                t_algorithm_e = time.time()

                self.algorithm_time += t_algorithm_e - t_algorithm_s

                if self.frame_counter % self.calculate_every == 0 and self.frame_counter > 0:
                    self.display_fps = self.calculate_every / self.display_time
                    self.algorithm_fps = self.calculate_every / self.algorithm_time

                    self.cpu_usage = hardware_status_meter.cpu_load['current']
                    self.gpu_usage = hardware_status_meter.gpu_load['current']

                    self.infer_time = self.algorithm.infer_time * 1000
                    self.pre_processing_time = self.algorithm.pre_processing_time * 1000
                    self.post_processing_time = self.algorithm.post_processing_time * 1000

                    self.algorithm_time = 0
                    self.display_time = 0

                rendered_frame = self._render_frame(frame, result)
                self.sink.write(rendered_frame)

                total_time = (
                    self.algorithm.pre_processing_time * 1000
                    + self.algorithm.infer_time * 1000
                    + self.algorithm.post_processing_time * 1000
                )
                additional_stats = {
                    'cpu_usage_current': hardware_status_meter.cpu_load['current'],
                    'gpu_usage_current': hardware_status_meter.gpu_load['current'],
                    'gpu_mem': hardware_status_meter.gpu_current_memory_gb['current'],
                    'preproc_time_current': self.algorithm.pre_processing_time * 1000,
                    'infer_time_current': self.algorithm.infer_time * 1000,
                    'postproc_time_current': self.algorithm.post_processing_time * 1000,
                    'total_time': total_time,
                }
                self._save_frame_results(self.frame_counter, result, additional_stats)

                t_display_e = time.time()
                t_display = t_display_e - t_display_s
                self.display_time += t_display
                self.frame_counter += 1

                if not finished_flag_queue.empty() and frame_queue.empty():
                    with open(self.output_json, 'w') as json_result:
                        json.dump(self.json_result, json_result, indent=2)

                    csv_prepared_json = self._transform_json_for_csv_conversion(self.json_result)
                    json_stats = pd.read_json(csv_prepared_json)
                    json_stats.to_csv(self.output_csv, index=False)
                    break

        return


def cli():
    parser = argparse.ArgumentParser(description='Bonseyes AI Asset video processing.')
    parser.add_argument('--model', '-mo', required=True, type=str, help='Path to model file')
    parser.add_argument(
        '--input-size',
        '-is',
        required=False,
        default='<default_input_size>',  # TODO add default input size
        type=str,
        help='Model input size in format: WIDTHxHEIGHT',
    )
    parser.add_argument(
        '--engine',
        '-en',
        nargs='?',
        const='pytorch',
        default='pytorch',
        choices=['pytorch', 'onnxruntime', 'tensorrt'],
        help='Inference engine: pytorch | onnxruntime | tensorrt',
    )
    parser.add_argument(
        '--device',
        '-de',
        nargs='?',
        const='cpu',
        default='cpu',
        choices=['cpu', 'gpu'],
        help='Available devices: cpu | gpu',
    )
    parser.add_argument(
        '--cpu-num',
        '-cn',
        required=False,
        default=None,
        type=int,
        help='Number of CPUs',
    )
    parser.add_argument(
        '--thread-num',
        '-tn',
        required=False,
        default=None,
        type=int,
        help='Number of threads',
    )
    parser.add_argument('--video-input', '-vi', required=True, type=str, help='Path to video input file or directory')
    parser.add_argument('--resize-width', '-rw', required=False, type=int, help='Resize width')
    parser.add_argument('--resize-height', '-rh', required=False, type=int, help='Resize height')
    parser.add_argument('--rotate', '-r', required=False, type=int, help='Rotate video frames: 90, -90, 180')
    parser.add_argument(
        '--convert-to-grayscale', '-ctg', required=False, dest='ctg', action='store_true', help='Convert to grayscale'
    )
    parser.add_argument(
        '--video-output', '-vo', required=True, type=str, help='Path to video output file or directory'
    )
    parser.add_argument('--json-output', '-jso', required=True, type=str, help='Path to json output file')
    parser.add_argument('--csv-output', '-csvo', required=True, type=str, help='Path to csv output file')
    parser.add_argument('--debug-info', '-di', required=False, default=False, action='store_true',
                        help='Render debug info')
    parser.add_argument('--logo', '-l', required=False, default=False, action='store_true', help='Render logo')

    return parser


def main():
    args = cli().parse_args()

    if args.cpu_num:
        p = psutil.Process()
        if psutil.cpu_count() < args.cpu_num:
            raise Exception(
                f"Specified number of CPUs exceeds available number. "
                f"Available CPUs on machine: {psutil.cpu_count()}"
            )
        cpu_list = [*range(0, args.cpu_num)]
        p.cpu_affinity(cpu_list)

    # TODO
    # Initialize algorithm
    algorithm = Algorithm(
        model_path=args.model,
        engine_type=args.engine,
        input_size=args.input_size,
        device=args.device,
        cpu_num=args.cpu_num,
        thread_num=args.thread_num
        # + additional arguments
    )

    inputs_ = []
    if not os.path.isdir(args.video_input):
        inputs_.append(args.video_input)
    else:
        for r, d, f in os.walk(args.video_input):
            for file in f:
                inputs_.append(os.path.join(r, file))

    for file_path in inputs_:
        file_name = file_path.split('/')[-1]
        input_ = os.path.abspath(file_path)

        output_video = input_.replace(args.video_input, args.video_output).split('/')
        output_video = '/'.join(output_video[:-1]) + '/processed_%s' % file_name
        os.makedirs(os.path.dirname(output_video), exist_ok=True)

        output_json = input_.replace(args.video_input, args.json_output).split('/')
        output_json = '/'.join(output_json[:-1]) + '/processed_%s' % file_name.replace('.mp4', '.json')
        os.makedirs(os.path.dirname(output_json), exist_ok=True)

        output_csv = input_.replace(args.video_input, args.csv_output).split('/')
        output_csv = '/'.join(output_csv[:-1]) + '/processed_%s' % file_name.replace('.mp4', '.csv')
        os.makedirs(os.path.dirname(output_csv), exist_ok=True)

        grayscale = True if args.ctg else False
        resize_w = args.resize_width
        resize_h = args.resize_height
        resize_pipe = f'videoscale ! video/x-raw,width={resize_w},height={resize_h} !' if resize_w else ''
        if args.rotate == 90:
            flip_option = 'clockwise'
        elif args.rotate == -90:
            flip_option = 'counterclockwise'
        elif args.rotate == 180:
            flip_option = 'rotate-180'
        rotate_pipe = f'videoflip method={flip_option} !' if args.rotate else ''

        capture_pipeline = capture_template % (input_, resize_pipe, rotate_pipe)
        sink_pipeline = sink_template % output_video

        capture = cv2.VideoCapture(capture_pipeline, cv2.CAP_GSTREAMER)
        capture_height = int(capture.get(cv2.CAP_PROP_FRAME_HEIGHT))
        capture_width = int(capture.get(cv2.CAP_PROP_FRAME_WIDTH))
        capture_fps = capture.get(cv2.CAP_PROP_FPS)
        capture_fourcc = cv2.VideoWriter_fourcc('a', 'v', 'i', '1')
        sink_size = (capture_width, capture_height)
        capture_color = True

        sink = cv2.VideoWriter(sink_pipeline, cv2.CAP_GSTREAMER, capture_fourcc, capture_fps, sink_size, capture_color)
        p = FrameProducerThread(capture=capture, grayscale=grayscale)
        c = InferenceAndDisplayThread(
            sink=sink,
            algorithm=algorithm,
            output_json=output_json,
            output_csv=output_csv,
            debug_info=args.debug_info,
            attach_logo=args.logo
        )

        hardware_status_meter.start()
        p.start()
        c.start()
        p.join()
        c.join()
        hardware_status_meter.collect()
        p.capture.release()
        cv2.destroyAllWindows()
        frame_queue.queue.clear()
        finished_flag_queue.queue.clear()

        algorithm.destroy()


if __name__ == '__main__':
    main()
