import argparse
import os
import sys
import subprocess

sys.path.append("/app/source/Submodule name")

from bonseyes_ aiassetgaze.utils.meter import HardwareStatusMeter
from bonseyes_ aiassetgaze.export.torch2onnx import Torch2ONNXConverter
try:
    from bonseyes_ aiassetgaze.export.onnx2trt import build_tensorrt_engine, GB, MB
except Exception as e:
    pass

ENGINES = ['onnxruntime', 'tensorrt']


def cli():
    parser = argparse.ArgumentParser(description='Bonseyes docker environment runner')
    parser.add_argument(
        '--precisions',
        '-p',
        nargs='+',
        required=True,
        type=str,
        help='Specify target precisions: fp16 fp32'
    )
    parser.add_argument(
        '--input-sizes', '-i', nargs='+', required=True, type=int, help='Specify target input sizes: s1 s2 ...'
    )
    parser.add_argument(
        '--engine',
        '-e',
        required=False,
        default=['all'],
        type=str,
        nargs='+',
        choices=['onnxruntime', 'tensorrt', 'all'],
        help='Choose one or more engines: onnxruntime | tensorrt | all',
    )
    parser.add_argument(
        '--backbone',
        '-bb',
        nargs='?',
        const='mobilenetv1',    # todo change available backbones
        default='mobilenetv1',
        choices=['mobilenetv1', 'mobilenetv0.5', 'resnet22'],
        help='Available backbones: mobilenetv1 | mobilenetv0.5 | resnet22',
    )
    parser.add_argument(
        '--workspace-unit',
        '-wu',
        nargs='?',
        const='GB',
        default='GB',
        choices=['MB', 'GB'],
        help='Available units: MB | GB',
    )
    parser.add_argument(
        '--workspace-size',
        '-ws',
        required=False,
        default=4,
        type=int,
        help='Conversion workspace size in GB',
    )
    parser.add_argument(
        '--enable-dla', '-ed', required=False, default=False, action='store_true', help='Enable dla for tensorrt model'
    )
    parser.add_argument(
        '--onnx-opset-version',
        '-op',
        default=11,
        nargs='?',
        type=int,
        help='ONNX opset version'
    )

    return parser


def main():
    args = cli().parse_args()

    hws = HardwareStatusMeter()
    gpu_name = hws.gpu_name
    input_sizes = [[ins, ins] for ins in args.input_sizes]
    precisions = args.precisions

    pytorch_models_root = f'/app/bonseyes_ aiassetgaze/models/pytorch/{args.backbone}'
    onnx_models_root = f'/app/bonseyes_ aiassetgaze/models/onnx/{args.backbone}'
    tensorrt_models_root = f'/app/bonseyes_ aiassetgaze/models/tensorrt/{gpu_name}/{args.backbone}'
    os.makedirs(onnx_models_root, exist_ok=True)
    os.makedirs(tensorrt_models_root, exist_ok=True)
    engines = ENGINES if args.engine == ['all'] else args.engine

    files = os.listdir(onnx_models_root)
    for f in files:
        if not f.endswith('.onnx'):
            continue
        os.remove(f'{onnx_models_root}/{f}')

    files = os.listdir(tensorrt_models_root)
    for f in files:
        if not f.endswith('.trt'):
            continue
        os.remove(f'{tensorrt_models_root}/{f}')

    for model in os.listdir(pytorch_models_root):
        if not model.endswith('.pkl') and not model.endswith('.pth'):
            continue

        model_path = f'{pytorch_models_root}/{model}'

        for size in input_sizes:
            sw = size[0]
            sh = size[1]
            model_prefix = model.rsplit('_', 2)[0]
            onnx_model_name = f"{model_prefix}_{sw}x{sh}_fp32.onnx"
            model_out_path = f'{onnx_models_root}/{onnx_model_name}'
            opset_arg = f'--opset-version {args.onnx_opset_version}' if args.onnx_opset_version else ''

            if 'onnxruntime' in engines:
                try:
                    subprocess.check_call(
                        f"""
                        python -m bonseyes_ aiassetgaze.export.torch2onnx \
                        --model-input {model_path} \
                        --model-output {model_out_path} \
                        --input-width {sw} \
                        --input-height {sh} \
                        --input-names None \
                        --output-names None \
                        --verbose \
                        --export-params \
                        --do-constant-folding \
                        {opset_arg}
                    """,
                        shell=True,
                    )
                except subprocess.CalledProcessError as e:
                    sys.exit(e.returncode)

            if 'tensorrt' in engines:
                for precision in precisions:
                    enable_dla = '--enable-dla' if args.enable_dla else ''
                    try:
                        subprocess.check_call(
                            f"""
                            python -m bonseyes_ aiassetgaze.export.onnx2trt \
                            --onnx-model {model_out_path} \
                            --output-dir {tensorrt_models_root} \
                            --precision {precision} \
                            --workspace-size {args.workspace_size} \
                            --workspace-unit {args.workspace_unit} \
                            {enable_dla}
                        """,
                            shell=True,
                        )
                    except subprocess.CalledProcessError as e:
                        sys.exit(e.returncode)


if __name__ == '__main__':
    main()
