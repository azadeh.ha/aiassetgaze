from abc import ABC, abstractmethod


class InferenceEngineBase(ABC):
    @abstractmethod
    def __init__(self):
        pass

    @abstractmethod
    def destroy(self):
        pass

    @abstractmethod
    def load(self, model):
        pass

    @abstractmethod
    def infer(self, session, tensor):
        pass
