import argparse
import subprocess
import sys
import os


def cli():
    parser = argparse.ArgumentParser(description='Bonseyes AI Asset export')
    parser.add_argument(
        '--input-sizes',
        '-i', nargs='+',
        required=True,
        default=['120x120'],
        type=str,
        help='Specify target input sizes: w1xh1 w2xh2 ...'
    )
    parser.add_argument(
        '--engine',
        '-e',
        required=False,
        default=['all'],
        type=str,
        nargs='+',
        choices=['onnxruntime', 'tensorrt', 'all'],
        help='Choose one or more engines: onnxruntime | tensorrt | all',
    )
    parser.add_argument(
        '--backbone',
        '-bb',
        nargs='?',
        const='mobilenetv1',
        default='mobilenetv1',
        choices=['mobilenetv1', 'mobilenetv0.5', 'resnet22'],   # todo change backbone choices
        help='Available backbones: mobilenetv1 | mobilenetv0.5 | resnet22',
    )
    parser.add_argument(
        '--workspace-unit',
        '-wu',
        nargs='?',
        const='GB',
        default='GB',
        choices=['MB', 'GB'],
        help='Available units: MB | GB',
    )
    parser.add_argument(
        '--workspace-size',
        '-ws',
        required=False,
        default=4,
        type=int,
        help='Conversion workspace size in GB',
    )
    parser.add_argument(
        '--enable-dla', '-ed', required=False, default=False, action='store_true', help='Enable dla for tensorrt model'
    )
    parser.add_argument(
        '--version',
        '-v',
        nargs='?',
        const='v1.0',
        default='v1.0',
        choices=['v1.0'],
        help='AI Asset version.'
    )

    return parser.parse_args()


def main():
    args = cli()

    if not os.path.exists('/app/bonseyes_ aiassetgaze/data/'):     # todo change the dataset path
        try:
            subprocess.check_call("python -m bonseyes_ aiassetgaze.data.get_data --data test", shell=True)
        except subprocess.CalledProcessError as e:
            sys.exit(e.returncode)

    input_sizes = ' '.join((ins.split("x")[0]) for ins in args.input_sizes)
    engine = ' '.join(args.engine)
    enable_dla = f'--enable-dla' if args.enable_dla else ''

    try:
        subprocess.check_call(
            f"""
            python -m bonseyes_ aiassetgaze.optimize.post_training_quantization.all \
              --input-sizes {input_sizes} \
              --engine {engine} \
              --backbone {args.backbone} \
              --workspace-unit {args.workspace_unit} \
              --workspace-size {args.workspace_size} \
              --version {args.version} \
              {enable_dla}
        """,
            shell=True,
        )
    except subprocess.CalledProcessError as e:
        sys.exit(e.returncode)


if __name__ == '__main__':
    main()
