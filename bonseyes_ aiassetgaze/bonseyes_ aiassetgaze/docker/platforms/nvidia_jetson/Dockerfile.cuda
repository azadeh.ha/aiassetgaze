ARG JETPACK_VERSION=4.4
ARG PYTORCH_VERSION=1.6
FROM nvcr.io/nvidia/l4t-pytorch:r32.${JETPACK_VERSION}-pth${PYTORCH_VERSION}-py3

ARG CMAKE_VERSION=3.16.5
ARG OPENCV_VERSION=4.5.0
ARG ONNXRUNTIME_VERSION=1.7.0
ARG NUM_JOBS=12

# ----------------------------------------------------------------------------------------------------------------------
# Install libraries
# ----------------------------------------------------------------------------------------------------------------------
ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update && apt-get -y --no-install-recommends install \
    build-essential \
    libcurl4-openssl-dev \
    zlib1g-dev \
    libssl-dev \
    libgtk-3-dev \
    libjpeg-dev \
    libpng-dev \
    libcanberra-gtk-module \
    libsm6 \
    libxext6 \
    libxrender-dev \
    libglib2.0-0 \
    libgtk2.0-0 \
    libxtst6 \
    libxss1 \
    libx11-6 \
    libgconf-2-4 \
    libnss3 \
    libasound2 \
    libgstreamer1.0-0 \
    libgstreamer1.0-dev \
    libgstreamer-plugins-base1.0-dev \
    libprotobuf-dev \
    libavcodec-dev \
    libavformat-dev \
    libswscale-dev \
    libopenexr-dev \
    libtiff-dev \
    libturbojpeg \
    libwebp-dev
# ----------------------------------------------------------------------------------------------------------------------

# ----------------------------------------------------------------------------------------------------------------------
# Install utility software
# ----------------------------------------------------------------------------------------------------------------------
RUN apt-get update && apt-get -y install --no-install-recommends \
    autoconf \
    automake \
    libtool \
    pkg-config \
    openssl \
    software-properties-common \
    gnupg  \
    apt-transport-https \
    ca-certificates \
    sudo \
    git \
    curl \
    unzip \
    bzip2 \
    wget \
    xvfb \
    xauth \
    protobuf-compiler \
    ffmpeg \
    gstreamer1.0-plugins-base \
    gstreamer1.0-plugins-good \
    gstreamer1.0-plugins-bad \
    gstreamer1.0-plugins-ugly \
    gstreamer1.0-libav \
    gstreamer1.0-tools \
    gstreamer1.0-x \
    gstreamer1.0-alsa \
    gstreamer1.0-gl \
    gstreamer1.0-gtk3 \
    gstreamer1.0-qt5 \
    gstreamer1.0-pulseaudio \
    language-pack-en \
    locales \
    locales-all
# ----------------------------------------------------------------------------------------------------------------------

# ----------------------------------------------------------------------------------------------------------------------
# Install cmake
# ----------------------------------------------------------------------------------------------------------------------
RUN wget https://github.com/Kitware/CMake/releases/download/v${CMAKE_VERSION}/cmake-${CMAKE_VERSION}.tar.gz && \
    tar -xvf cmake-${CMAKE_VERSION}.tar.gz && \
    cd cmake-${CMAKE_VERSION} && \
    ./bootstrap --system-curl && \
    make && \
    make install
# ----------------------------------------------------------------------------------------------------------------------

# ----------------------------------------------------------------------------------------------------------------------
# Base python packages
# ----------------------------------------------------------------------------------------------------------------------
RUN python3 -m pip install --upgrade pip && \
    python3 -m pip install --user wheel setuptools numpy==1.19.4 Cython==0.29.23
# ----------------------------------------------------------------------------------------------------------------------

# ----------------------------------------------------------------------------------------------------------------------
# Install OpenCV
# ----------------------------------------------------------------------------------------------------------------------
RUN cd /tmp && \
    wget -O opencv.zip https://github.com/opencv/opencv/archive/${OPENCV_VERSION}.zip && \
    wget -O opencv_contrib.zip https://github.com/opencv/opencv_contrib/archive/${OPENCV_VERSION}.zip && \
    wget -O opencv_extra.zip https://github.com/opencv/opencv_extra/archive/${OPENCV_VERSION}.zip && \
    unzip opencv.zip && \
    unzip opencv_contrib.zip && \
    unzip opencv_extra.zip && \
    mkdir -p build && cd build && \
    cmake -DOPENCV_EXTRA_MODULES_PATH=../opencv_contrib-${OPENCV_VERSION}/modules \
        -D CMAKE_USE_OPENSSL=ON \
        -DCMAKE_BUILD_TYPE=Release \
        -DCMAKE_INSTALL_PREFIX=/usr \
        -DBUILD_PNG=OFF \
        -DBUILD_TIFF=OFF \
        -DBUILD_TBB=OFF \
        -DBUILD_JPEG=OFF \
        -DBUILD_JASPER=OFF \
        -DBUILD_ZLIB=OFF \
        -DBUILD_EXAMPLES=ON \
        -DBUILD_JAVA=OFF \
        -DBUILD_opencv_python2=OFF \
        -DBUILD_opencv_python3=ON \
        -DCMAKE_INSTALL_PREFIX=$(python3 -c "import sys; print(sys.prefix)") \
        -DPYTHON3_EXECUTABLE=$(which python3) \
        -DPYTHON3_INCLUDE_DIR=$(python3 -c "from distutils.sysconfig import get_python_inc; print(get_python_inc())") \
        -DPYTHON3_PACKAGES_PATH=$(python3 -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())") \
        -DWITH_OPENCL=OFF \
        -DWITH_OPENMP=OFF \
        -DWITH_FFMPEG=ON \
        -DWITH_GSTREAMER=ON \
        -DWITH_GSTREAMER_0_10=OFF \
        -DWITH_CUDA=OFF \
        -DWITH_GTK=ON \
        -DWITH_VTK=OFF \
        -DWITH_TBB=ON \
        -DWITH_1394=OFF \
        -DWITH_OPENEXR=OFF \
        # Disable CUDA as nvidia doesnt support cv implementation
        #-DCUDA_TOOLKIT_ROOT_DIR=/usr/local/cuda \
        #-DCUDA_ARCH_PTX="" \
        -DINSTALL_C_EXAMPLES=OFF \
        -DINSTALL_TESTS=OFF \
        -DOPENCV_TEST_DATA_PATH=../opencv_extra-${OPENCV_VERSION}/testdata \
        ../opencv-${OPENCV_VERSION} && \
    sudo cmake --build . --parallel ${NUM_JOBS} && \
    sudo make install && \
    sudo ldconfig && \
    sudo rm -rf /tmp/*
# ----------------------------------------------------------------------------------------------------------------------

RUN apt-get install -y python3-pip

# ----------------------------------------------------------------------------------------------------------------------
# Set default Python
# ----------------------------------------------------------------------------------------------------------------------
RUN update-alternatives --install /usr/bin/python python /usr/bin/python3.6 10
# ----------------------------------------------------------------------------------------------------------------------

WORKDIR /app

# ----------------------------------------------------------------------------------------------------------------------
# Install torch and torchvision
# ----------------------------------------------------------------------------------------------------------------------
#ENV BUILD_VERSION=0.9.0
#RUN apt-get install -y libopenblas-dev libopenmpi-dev libomp-dev
#RUN cd /tmp && \
#    python3 -m pip install gdown && \
#    gdown https://drive.google.com/uc?id=1-XmTOEN0z1_-VVCI3DPwmcdC-eLT_-n3 && \
#    python3 -m pip install torch-1.8.0a0+37c1f4a-cp36-cp36m-linux_aarch64.whl && \
#    rm -rf /tmp/*
#
#RUN cd /tmp && \
#    apt-get install -y libjpeg-dev zlib1g-dev libpython3-dev libavcodec-dev libavformat-dev libswscale-dev && \
#    gdown https://drive.google.com/uc?id=1BdvXkwUGGTTamM17Io4kkjIT6zgvf4BJ && \
#    python3 -m pip install torchvision-0.9.0a0+01dfa8e-cp36-cp36m-linux_aarch64.whl && \
#    rm -rf /tmp/*
# ----------------------------------------------------------------------------------------------------------------------

ENV LD_PRELOAD /usr/lib/aarch64-linux-gnu/libgomp.so.1

# ----------------------------------------------------------------------------------------------------------------------
# Setup AI Asset
# ----------------------------------------------------------------------------------------------------------------------
ADD . /app

# pre install
# If you are using submodule and want to apply a patch, try to use or modify this line
# RUN cd /app/source/Submodule name && git apply /app/source/patch/modification_1.patch
RUN python3 -m pip install --user -e .[nvidia_jetson]

# post install
RUN cd /app && sudo rm -rf .git && \
    sudo chmod -R +x /app/interface/

# uninstall onnxruntime cpu after installation onnx-simplifier (onnxruntime-gpu is not recognized after this)
RUN pip uninstall -y onnxruntime
# ----------------------------------------------------------------------------------------------------------------------

# ----------------------------------------------------------------------------------------------------------------------
# Install Onnxruntime
# ----------------------------------------------------------------------------------------------------------------------
RUN cd /tmp && \
    wget https://nvidia.box.com/shared/static/ukszbm1iklzymrt54mgxbzjfzunq7i9t.whl -O onnxruntime_gpu-1.7.0-cp36-cp36m-linux_aarch64.whl && \
    python3 -m pip install onnxruntime_gpu-1.7.0-cp36-cp36m-linux_aarch64.whl
# ----------------------------------------------------------------------------------------------------------------------
