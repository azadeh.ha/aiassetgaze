import argparse
import subprocess
import sys

from bonseyes_ aiassetgaze.utils.meter import HardwareStatusMeter


def cli():
    parser = argparse.ArgumentParser(description='Bonseyes AI Asset camera demo')
    parser.add_argument(
        '--input-size',
        '-is',
        required=False,
        default='120x120',
        type=str,
        help='Model input size in format: WIDTHxHEIGHT',
    )
    parser.add_argument(
        '--engine',
        nargs='?',
        const='pytorch',
        default='pytorch',
        choices=['pytorch', 'onnxruntime', 'tensorrt'],
        help='Inference engine: pytorch | onnxruntime | tensorrt',
    )
    parser.add_argument(
        '--backbone',
        '-bb',
        nargs='?',
        const='mobilenetv1',
        default='mobilenetv1',
        choices=['mobilenetv1', 'mobilenetv0.5', 'resnet22'],   # todo change backbone choices
        help='Available backbones: mobilenetv1 | mobilenetv0.5 | resnet22',
    )
    parser.add_argument(
        '--device',
        '-de',
        nargs='?',
        const='cpu',
        default='cpu',
        choices=['cpu', 'gpu'],
        help='Available devices: cpu | gpu',
    )
    parser.add_argument(
        '--precision',
        required=False,
        nargs='?',
        const='fp32',
        default='fp32',
        choices=['fp32', 'fp16', 'int8'],
        help='Model precision: fp32, fp16. Default: fp32'
    )
    parser.add_argument(
        '--version',
        '-v',
        nargs='?',
        const='v1.0',
        default='v1.0',
        choices=['v1.0'],
        help='AI Asset version.'
    )
    parser.add_argument(
        '--cpu-num',
        '-cn',
        required=False,
        default=None,
        type=int,
        help='Number of CPUs',
    )
    parser.add_argument(
        '--thread-num',
        '-tn',
        required=False,
        default=None,
        type=int,
        help='Number of threads',
    )
    parser.add_argument(
        '--single-face-track',
        '-sft',
        required=False,
        default=False,
        dest='single_face_track',
        action='store_true',
        help="Track single face.",
    )
    parser.add_argument(
        '--safety-check',
        '-sc',
        required=False,
        type=int,
        default=0,
        choices=range(0, 51),
        metavar="[0-50]",
        help="Safety face detection check on frame interval [0-50] frames.",
    )
    parser.add_argument('--camera-id', default=0, type=int, help='ID of camera to be used')

    return parser.parse_args()


def main():
    args = cli()

    input_size = args.input_size.split('x')[0]

    cpu_num = f"--cpu-num {args.cpu_num}" if args.cpu_num else ''
    thread_num = f"--thread-num {args.thread_num}" if args.thread_num else ''

    hws = HardwareStatusMeter()
    gpu_name = hws.gpu_name

    model_path = '/app/bonseyes_ aiassetgaze/models'

    if args.engine == 'pytorch':
        model_path += f'/{args.engine}'
        model_path += f'/{args.backbone}'
        model_path += f'/{args.version}_{args.backbone}_default_{args.input_size}_fp32.pth'
    elif args.engine == 'onnxruntime':
        model_path += '/onnx'
        model_path += f'/{args.backbone}'
        model_path += f'/{args.version}_{args.backbone}_default_{args.input_size}_{args.precision}.onnx'
    elif args.engine == 'tensorrt':
        model_path += f'/{args.engine}'
        model_path += f'/{gpu_name}'
        model_path += f'/{args.backbone}'
        model_path += f'/{args.version}_{args.backbone}_default_{args.input_size}_{args.precision}_dla_disabled.trt'

    try:
        subprocess.check_call(
            f"""
            python -m bonseyes_ aiassetgaze.process.camera \
              --model "{model_path}" \
              --input-size {input_size} \
              --engine {args.engine} \
              --backbone {args.backbone} \
              --device {args.device} \
              {cpu_num} \
              {thread_num} \
              --camera-id {args.camera_id} \
              {'--single-face-track' if args.single_face_track else ''} \
              --video-output /app/recording.mp4 \
              --json-output /app/recording_predictions.json \
              --csv-output /app/recording_predictions.csv
        """,
            shell=True,
        )
    except subprocess.CalledProcessError as e:
        sys.exit(e.returncode)


if __name__ == '__main__':
    main()
