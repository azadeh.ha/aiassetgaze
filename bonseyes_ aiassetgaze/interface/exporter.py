import argparse
import subprocess
import sys


def cli():
    parser = argparse.ArgumentParser(description='Bonseyes AI Asset export')
    parser.add_argument(
        '--input-sizes',
        '-i', nargs='+',
        required=True,
        default=['120x120'],
        type=str,
        help='Specify target input sizes: w1xh1 w2xh2 ...'
    )
    parser.add_argument(
        '--engine',
        '-e',
        required=False,
        default=['all'],
        type=str,
        nargs='+',
        choices=['onnxruntime', 'tensorrt', 'all'],
        help='Choose one or more engines: onnxruntime | tensorrt | all',
    )
    parser.add_argument(
        '--backbone',
        '-bb',
        nargs='?',
        const='mobilenetv1',
        default='mobilenetv1',
        choices=['mobilenetv1', 'mobilenetv0.5', 'resnet22'],   # todo change backbone choices
        help='Available backbones: mobilenetv1 | mobilenetv0.5 | resnet22',
    )
    parser.add_argument(
        '--precisions',
        required=True,
        nargs='+',
        type=str,
        default='fp32',
        choices=['fp16', 'fp32'],
        help='Model precision: fp32, fp16. Default: fp32'
    )
    parser.add_argument(
        '--workspace-unit',
        '-wu',
        nargs='?',
        const='GB',
        default='GB',
        choices=['MB', 'GB'],
        help='Available units: MB | GB',
    )
    parser.add_argument(
        '--workspace-size',
        '-ws',
        required=False,
        default=4,
        type=int,
        help='Conversion workspace size in GB',
    )
    parser.add_argument(
        '--enable-dla', '-ed', required=False, default=False, action='store_true', help='Enable dla for tensorrt model'
    )

    return parser.parse_args()


def main():
    args = cli()

    input_sizes = ' '.join((ins.split("x")[0]) for ins in args.input_sizes)
    engine = ' '.join(args.engine)
    precisions = ' '.join(args.precisions)
    enable_dla = f'--enable-dla' if args.enable_dla else ''

    try:
        subprocess.check_call(
            f"""
            python -m bonseyes_ aiassetgaze.export.all \
              --input-sizes {input_sizes} \
              --engine {engine} \
              --backbone {args.backbone} \
              --precisions {precisions} \
              --workspace-unit {args.workspace_unit} \
              --workspace-size {args.workspace_size} \
              {enable_dla}
        """,
            shell=True,
        )
    except subprocess.CalledProcessError as e:
        sys.exit(e.returncode)


if __name__ == '__main__':
    main()
