import argparse
import json
import os
import sys
import subprocess
import pandas as pd

from bonseyes_ aiassetgaze.utils.meter import HardwareStatusMeter

ENGINES = ['pytorch', 'onnxruntime', 'tensorrt']
TEST_DATASETS = []  # todo List of test datasets


def cli():
    parser = argparse.ArgumentParser(description='Bonseyes AI Asset benchmark.')
    parser.add_argument(
        '--dataset',
        '-da',
        nargs='?',
        required=True,
        type=str,
        choices=TEST_DATASETS + ['all'],
        help=f"Available devices: {TEST_DATASETS + ['all']}",
    )
    parser.add_argument(
        '--engine',
        '-e',
        required=True,
        default=['all'],
        type=str,
        nargs='+',
        choices=ENGINES + ['all'],
        help=f"Choose one or more engines: {ENGINES + ['all']}",
    )
    parser.add_argument(
        '--backbone',
        '-bb',
        nargs='?',
        const='mobilenetv1',
        default='mobilenetv1',
        choices=['mobilenetv1', 'mobilenetv0.5', 'resnet22'],   # todo change backbone choices
        help='Available backbones: mobilenetv1 | mobilenetv0.5 | resnet22',
    )
    parser.add_argument('--input-sizes', '-is', required=True, type=int, nargs='+', help='Model input size')
    parser.add_argument(
        '--warm-up-num',
        '-wn',
        default=100,
        required=False,
        type=int,
        help='Number of images for a warm up.',
    )
    parser.add_argument(
        '--device',
        '-de',
        nargs='?',
        const='cpu',
        default='cpu',
        choices=['cpu', 'gpu'],
        help='Available devices: cpu | gpu',
    )
    parser.add_argument(
        '--cpu-num',
        '-cn',
        required=False,
        default=None,
        type=int,
        help='Number of CPUs',
    )
    parser.add_argument(
        '--thread-num',
        '-tn',
        required=False,
        default=None,
        type=int,
        help='Number of threads',
    )

    return parser


def main():
    args = cli().parse_args()

    if args.cpu_num is not None:
        cpu_num = f'--cpu-num {args.cpu_num}'
    else:
        cpu_num = ''

    if args.thread_num is not None:
        thread_num = f'--thread-num {args.thread_num}'
    else:
        thread_num = ''

    datasets = TEST_DATASETS if args.dataset == 'all' else [args.dataset]
    engines = ENGINES if args.engine == ['all'] else args.engine
    hws = HardwareStatusMeter()
    gpu_name = hws.gpu_name
    input_sizes = args.input_sizes
    dataset_path = '/app/bonseyes_ aiassetgaze/data/<test_data>/'
    pytorch_models_root = '/app/bonseyes_ aiassetgaze/models/pytorch/mobilenetv1/'
    onnxruntime_models_root = '/app/bonseyes_ aiassetgaze/models/onnx/mobilenetv1/'
    tensorrt_models_root = f'/app/bonseyes_ aiassetgaze/models/tensorrt/{gpu_name}/mobilenetv1/'
    result_dir = '/app/eval_results'
    results = []

    # Evaluate TensorRT models
    if 'tensorrt' in engines:
        for size in input_sizes:
            model_list = [model for model in os.listdir(tensorrt_models_root) if model.endswith('.trt') and
                          str(size) in model and args.backbone in model]
            if not model_list:
                raise Exception(f'Tensorrt {args.backbone} model of {size} input size does not exist!')

            for model in model_list:
                model_path = f"{tensorrt_models_root}/{model}"
                for dataset in datasets:
                    try:
                        subprocess.check_call(
                            f"""
                            python -m bonseyes_ aiassetgaze.benchmark.evaluate \
                              --model {model_path} \
                              --input-size {size} \
                              --dataset {dataset} \
                              --engine tensorrt \
                              --backbone {args.backbone} \
                              --device {args.device} \
                              --warm-up-num {args.warm_up_num}
                              {cpu_num} \
                              {thread_num}
                        """,
                            shell=True,
                        )
                    except subprocess.CalledProcessError as e:
                        sys.exit(e.returncode)

                    with open(f'{result_dir}/result_{dataset}.json') as result_file:
                        result = json.load(result_file)
                    results.append(result)

    # Evaluate PyTorch models
    if 'pytorch' in engines:
        for size in input_sizes:
            model_list = [model for model in os.listdir(pytorch_models_root) if model.endswith(('.pth','.pkl')) and
                          str(size) in model and args.backbone in model]
            if not model_list:
                raise Exception(f'Pytorch {args.backbone} model of {size} input size does not exist!')

            for model in model_list:
                model_path = f"{pytorch_models_root}/{model}"
                for dataset in datasets:
                    try:
                        subprocess.check_call(
                            f"""
                            python -m bonseyes_ aiassetgaze.benchmark.evaluate \
                              --model {model_path} \
                              --input-size {size} \
                              --dataset {dataset} \
                              --engine pytorch \
                              --backbone {args.backbone} \
                              --device {args.device} \
                              --warm-up-num {args.warm_up_num}
                              {cpu_num} \
                              {thread_num}
                        """,
                            shell=True,
                        )
                    except subprocess.CalledProcessError as e:
                        sys.exit(e.returncode)

                    with open(f'{result_dir}/result_{dataset}.json') as result_file:
                        result = json.load(result_file)
                    results.append(result)

    # Evaluate ONNX models
    if 'onnxruntime' in engines:
        for size in input_sizes:
            model_list = [model for model in os.listdir(onnxruntime_models_root) if model.endswith('.onnx') and
                          str(size) in model and args.backbone in model]
            if not model_list:
                raise Exception(f'Onnx {args.backbone} model of {size} input size does not exist!')

            for model in model_list:
                model_path = f"{onnxruntime_models_root}/{model}"
                for dataset in datasets:
                    try:
                        subprocess.check_call(
                            f"""
                            python -m bonseyes_ aiassetgaze.benchmark.evaluate \
                              --model {model_path} \
                              --input-size {size} \
                              --dataset {dataset} \
                              --engine onnxruntime \
                              --backbone {args.backbone} \
                              --device {args.device} \
                              --warm-up-num {args.warm_up_num}
                              {cpu_num} \
                              {thread_num}
                        """,
                            shell=True,
                        )
                    except subprocess.CalledProcessError as e:
                        sys.exit(e.returncode)

                    with open(f'{result_dir}/result_{dataset}.json') as result_file:
                        result = json.load(result_file)
                    results.append(result)

    with open('/app/benchmark.json', 'w') as json_benchmark:
        json.dump(results, json_benchmark, indent=2)

    json_obj = json.dumps(results)
    json_stats = pd.read_json(json_obj, convert_dates=False)
    json_stats.to_csv('/app/benchmark.csv', index=False)


if __name__ == '__main__':
    main()
