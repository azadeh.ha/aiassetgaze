import argparse
import datetime
import time
import cv2
import os
import re
import sys
import json
import psutil
import numpy as np
import platform
from tqdm import tqdm

from bonseyes_ aiassetgaze.algorithm.algorithm import Algorithm, AlgorithmInput
from bonseyes_ aiassetgaze.utils.meter import HardwareStatusMeter
from bonseyes_ aiassetgaze.benchmark.model_summary import ModelSummarizer

TEST_DATASETS = []  # List of test datasets

# environment detection (rpi, jetson, x86)
def _detect_environment():
    detected_env = ''
    if platform.machine() == 'x86_64':
        detected_env = 'x86_64'

    if platform.machine() == 'aarch64':
        if os.environ.get('JETSON_MACHINE') is not None:
            detected_env = os.environ.get('JETSON_MACHINE')
        elif 'tegra' in os.uname().release:
            detected_env = 'Jetson'
        if 'raspi' in os.uname().release:
            detected_env = 'raspberrypi'

    return detected_env

def benchmark(
    dataset,
    dataset_path,
    algorithm,
    model,
    engine,
    backbone,
    input_size,
    device,
    number_cpus,
    number_threads,
    warm_up_num
):
    """
    Benchmark function that will evaluate model and store the
    """
    time_start = time.time()
    hw_meter = HardwareStatusMeter(sample_rate_sec=1)
    hw_meter.start()
    platform = _detect_environment()
    avg_inference_time = 0

    # TODO
    # Implement or reuse benchmark for AI Asset

    model_summarizer = ModelSummarizer(
        model_path=model,
        engine=engine,
        backbone=backbone,
        input_size=input_size
    )
    model_stats = model_summarizer.get_model_summary()
    detailed_hw_stats, summary_hw_stats = hw_meter.collect()
    # gflops_per_second = '-'
    # if model_stats['GFLOPs'] != '-':
    #     gflops_per_second = float(model_stats['GFLOPs']) / avg_inference_time * 1000

    energy_efficiency = '-'
    # if 'Jetson' in platform and gflops_per_second != '-':
    #     energy_efficiency = str(gflops_per_second / float(summary_hw_stats['power (W)']))

    time_end = time.time()
    eval_time = time_end - time_start
    eval_time_formatted = str(datetime.timedelta(seconds=eval_time))
    #TODO add accuracy stats
    accuracy_stats = {}
    result = {
        'NAME': None,
        'DATASET': None,
        'BACKBONE': None,
        'INPUT_SIZE': None,
        'ENGINE': None,
        'PRECISION': None,
        'NME': None,
        'STD': None,
        'STORAGE': None,
        'LATENCY': None,
        'PREPROCESSING TIME': None,
        'INFERENCE TIME': None,
        'POSTPROCESSING TIME': None,
        'DEVICE': None,
        'CPU_NUM': None,
        'THREAD_NUM': None,
        'GPU_MEMORY': None,
        'EVAL_TIME': eval_time_formatted,
    }
    hw_stats = {
        'CPU_MEM': '%.1f' % summary_hw_stats['cpu_memory (%)'],
        'CPU_MEM_PEAK': '%.1f' % summary_hw_stats['cpu_memory_peak (GB)'],
        'GPU_MEM': '%.1f' % summary_hw_stats['gpu_memory (%)'],
        'GPU_MEM_PEAK': '%.1f' % summary_hw_stats['gpu_memory_peak (GB)'],
        'MEMORY_BANDWIDTH': '%.1f' % summary_hw_stats['memory_bw (%)'],
        'MEMORY_BANDWIDTH_PEAK': '%.1f' % summary_hw_stats['memory_bw_peak (%)'],
        'CPU_LOAD': '%.1f' % summary_hw_stats['cpu_load (%)'],
        'GPU_LOAD': '%.1f' % summary_hw_stats['gpu_load (%)'],
        'NPU_LOAD': '%.1f' % summary_hw_stats['npu_load (%)'],
        'CPU_TEMP': '%.1f' % summary_hw_stats['cpu_temperature (C)'],
        'GPU_TEMP': '%.1f' % summary_hw_stats['gpu_temperature (C)'],
        'POWER_CONSUMPTION': '%.1f' % summary_hw_stats['power (W)'],
        'ENERGY_EFFICIENCY': energy_efficiency
    }

    results = {**result, **accuracy_stats, **hw_stats}

    return results


def cli():
    parser = argparse.ArgumentParser(description='Bonseyes AI Asset evaluation.')
    parser.add_argument('--model', '-mo', required=True, type=str, help='Path to model file')
    parser.add_argument('--input-size', '-is', required=True, type=int, nargs='+', help='Model input size')
    parser.add_argument(
        '--dataset',
        '-da',
        nargs='?',
        required=True,
        type=str,
        choices=TEST_DATASETS + ['all'],
        help=f"Available devices: {TEST_DATASETS + ['all']}",
    )
    parser.add_argument(
        '--engine',
        '-e',
        required=True,
        default='pytorch',
        type=str,
        help='Inference engine: pytorch | onnxruntime | tensorrt',
    )
    parser.add_argument(
        '--device',
        '-de',
        nargs='?',
        const='cpu',
        default='cpu',
        choices=['cpu', 'gpu'],
        help='Available devices: cpu | gpu',
    )
    parser.add_argument(
        '--warm-up-num',
        '-wn',
        default=100,
        required=False,
        type=int,
        help='Number of images for a warm up.',
    )
    parser.add_argument(
        '--cpu-num',
        '-cn',
        required=False,
        default=None,
        type=int,
        help='Number of CPUs',
    )
    parser.add_argument(
        '--thread-num',
        '-tn',
        required=False,
        default=None,
        type=int,
        help='Number of threads',
    )

    return parser


def main():
    args = cli().parse_args()

    algorithm = Algorithm(
        model_path=args.model,
        engine_type=args.engine,
        input_size=args.input_size,
        device=args.device,
        cpu_num=args.cpu_num,
        thread_num=args.thread_num,
    )

    number_threads = '-'
    if args.thread_num is not None:
        os.environ["OMP_NUM_THREADS"] = f'{args.thread_num}'
        if args.device == 'cpu':
            number_threads = args.thread_num

    number_cpus = '-'
    if args.cpu_num:
        p = psutil.Process()
        if psutil.cpu_count() < args.cpu_num:
            raise Exception(
                f"Specified number of CPUs exceeds available number. "
                f"Available CPUs on machine: {psutil.cpu_count()}"
            )
        cpu_list = [*range(0, args.cpu_num)]
        p.cpu_affinity(cpu_list)
        if args.device == 'cpu':
            number_cpus = args.cpu_num

    datasets = TEST_DATASETS if args.dataset == 'all' else [args.dataset]
    for dataset in datasets:
        # TODO
        # Add proper benchmark arguments
        result = benchmark()
        algorithm.destroy()
        print(f"{args.dataset.upper()} results: ")
        print(result)

        result_dir = '/app/eval_results'
        if not os.path.exists(result_dir):
            os.makedirs(result_dir)

        with open(f'{result_dir}/result_{dataset}.json', 'w') as json_result:
            json.dump(result, json_result, indent=2)


if __name__ == '__main__':
    main()