import argparse
import yaml
import os


def cli():
    parser = argparse.ArgumentParser(description='Bonseyes AI Asset platform docker images builder.')
    parser.add_argument(
        '--platform',
        '-pt',
        nargs='?',
        required=True,
        choices=['x86_64', 'nvidia_jetson', 'rpi', 'imx'],
        help='Available platforms: x86_64 | nvidia_jetson | rpi | imx',
    )
    parser.add_argument(
        '--device',
        '-de',
        nargs='?',
        required=True,
        choices=['cpu', 'cuda'],
        help='Available devices: cpu | cuda',
    )
    parser.add_argument('--image-name', '-inm', required=True, type=str, help='Specify image name for built image')
    parser.add_argument('--profile', '-p', required=True, type=str, help='Path to environment profile')
    args = parser.parse_args()

    return args


def main():
    args = cli()
    image_name = args.image_name
    platform = args.platform
    device = args.device
    with open(args.profile, 'r') as file:
        profile = yaml.load(file, Loader=yaml.FullLoader)

    dockerfile_path = f'bonseyes_ aiassetgaze/docker/platforms/{platform}/Dockerfile.{device}'

    if platform == 'x86_64':
        base_image = profile['build']['versions'].get('tensorrt_base', None) if not None else ''
        onnx_version = profile['build']['versions']['onnx']
        cmake_version = profile['build']['versions']['cmake']
        opencv_version = profile['build']['versions']['opencv']
        onnxruntime_version = profile['build']['versions']['onnxruntime']
        python_version = profile['build']['versions']['python']

        os.system(
            f'''
            docker build \
                --build-arg TENSORRT_BASE={base_image} \
                --build-arg PY_VERSION={python_version} \
                --build-arg CMAKE_VERSION={cmake_version} \
                --build-arg OPENCV_VERSION={opencv_version} \
                --build-arg ONNX_VERSION={onnx_version} \
                --build-arg ONNXRUNTIME_VERSION={onnxruntime_version} \
                -t {image_name} -f {dockerfile_path} . 
        '''
        )

    if platform == 'nvidia_jetson':
        jetpack_version = profile['build']['versions']['jetpack']
        pytorch_version = profile['build']['versions']['pytorch']
        cmake_version = profile['build']['versions']['cmake']
        opencv_version = profile['build']['versions']['opencv']
        onnxruntime_version = profile['build']['versions']['onnxruntime']

        os.system(
            f'''
            docker build \
                --build-arg JETPACK_VERSION={jetpack_version} \
                --build-arg PYTORCH_VERSION={pytorch_version} \
                --build-arg CMAKE_VERSION={cmake_version} \
                --build-arg OPENCV_VERSION={opencv_version} \
                --build-arg ONNXRUNTIME_VERSION={onnxruntime_version} \
                -t {image_name} -f {dockerfile_path} . 
        '''
        )

    if platform == 'rpi':
        arch = profile['build']['arch']
        cmake_version = profile['build']['versions']['cmake']
        opencv_version = profile['build']['versions']['opencv']
        onnxruntime_version = profile['build']['versions']['onnxruntime']
        python_version = profile['build']['versions']['python']

        os.system(
            f'''
            docker buildx build --platform {arch} \
                --build-arg CMAKE_VERSION={cmake_version} \
                --build-arg OPENCV_VERSION={opencv_version} \
                --build-arg ONNXRUNTIME_VERSION={onnxruntime_version} \
                --build-arg PY_VERSION={python_version} \
                -t {image_name} -f {dockerfile_path} . 
        '''
        )


if __name__ == '__main__':
    main()
