#pragma once

#include <iostream>
#include <vector>
#include <string>

#include "deps/inference_engines/include/tensorrt_wrapper.hpp"
#include "preprocessing/include/preprocessor.hpp"
#include "postprocessing/include/postprocessor.hpp"


using namespace std;


class Algorithm {
    private:
        TensorRTInferenceWrapper<float> *_inference_engine;

        vector<float> _preprocess(cv::Mat &input_image) {
            return vector<float>{};
        }

        vector<vector<float>> _infer(vector<float> &preprocessed_image) {
            return _inference_engine->forward_sync(preprocessed_image);
        }

        bool _postprocess(vector<vector<float>> forward_out) {
            return true;
        }

    public:
         Algorithm(string &model_path) {
            _inference_engine = new TensorRTInferenceWrapper<float>(model_path);
        }

        bool process(cv::Mat input_image) {
            vector<float> image = _preprocess(input_image);
            vector<vector<float>> infer_out = _infer(image);

            return _postprocess(infer_out);
        }
};