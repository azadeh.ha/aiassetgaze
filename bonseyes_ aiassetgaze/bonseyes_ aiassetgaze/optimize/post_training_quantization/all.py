import argparse
import os
import sys
import subprocess

from onnxruntime.quantization import quantize_static, CalibrationDataReader, QuantFormat, QuantType
from bonseyes_ aiassetgaze.optimize.post_training_quantization.onnx_quantize import DataReader
from bonseyes_ aiassetgaze.optimize.post_training_quantization.calibration_data import calibration_dataloader
from bonseyes_ aiassetgaze.utils.meter import HardwareStatusMeter

sys.path.append("/app/source/Submodule name")

try:
    from bonseyes_ aiassetgaze.optimize.post_training_quantization.trt_quantize import INT8Calibrator
    from bonseyes_ aiassetgaze.export.onnx2trt import build_tensorrt_engine, GB, MB
except Exception as e:
    pass

ENGINES = ['onnxruntime', 'tensorrt']


def cli():
    parser = argparse.ArgumentParser(description='Bonseyes AI Asset static quantization tool.')
    parser.add_argument(
        '--input-sizes', '-i', nargs='+', required=True, type=int, help='Specify target input sizes: s1 s2 ...'
    )
    parser.add_argument(
        '--engine',
        '-e',
        required=False,
        default=['all'],
        type=str,
        nargs='+',
        choices=['onnxruntime', 'tensorrt', 'all'],
        help='Choose one or more engines: onnxruntime | tensorrt | all',
    )
    parser.add_argument(
        '--backbone',
        '-bb',
        nargs='?',
        const='', # TODO add default backbone
        default='', # TODO add default backbone
        # choices=['mobilenetv1', '...'],  # TODO add available backbones
        help='Available backbones: mobilenetv1 | ... ',
    )
    parser.add_argument(
        '--workspace-unit',
        '-wu',
        nargs='?',
        const='GB',
        default='GB',
        choices=['MB', 'GB'],
        help='Available units: MB | GB',
    )
    parser.add_argument(
        '--workspace-size',
        '-ws',
        required=False,
        default=4,
        type=int,
        help='Conversion workspace size in GB',
    )
    parser.add_argument(
        '--enable-dla', '-ed', required=False, default=False, action='store_true', help='Enable dla for tensorrt model'
    )
    parser.add_argument(
        "--calibrate-dataset",
        default="/app/<calibration_data_path>",  # TODO add calibration data path
        help="Calibration data set"
    )
    parser.add_argument(
        '--version',
        '-v',
        nargs='?',
        const='v1.0',
        default='v1.0',
        choices=['v1.0'],
        help='AI Asset version.'
    )

    return parser


def main():
    args = cli().parse_args()

    hws = HardwareStatusMeter()
    gpu_name = hws.gpu_name
    input_sizes = [[ins, ins] for ins in args.input_sizes]

    onnx_models_root = f'/app/bonseyes_ aiassetgaze/models/onnx/{args.backbone}'
    tensorrt_models_root = f'/app/bonseyes_ aiassetgaze/models/tensorrt/{gpu_name}/{args.backbone}'
    os.makedirs(onnx_models_root, exist_ok=True)
    os.makedirs(tensorrt_models_root, exist_ok=True)
    engines = ENGINES if args.engine == ['all'] else args.engine

    for size in input_sizes:
        model_list = [model for model in os.listdir(onnx_models_root) if model.endswith('fp32.onnx') and
                      str(size[0]) in model and args.version in model and args.backbone in model]
        if not model_list:
            raise Exception(f'Onnx {args.backbone} model of {size} input size does not exist!')

        for model in model_list:
            sw = size[0]
            sh = size[1]
            model_prefix = model.rsplit('_', 2)[0]
            onnx_model_path = f'{onnx_models_root}/{model}'

            if 'onnxruntime' in engines:
                model_out_path = f'{onnx_models_root}/{model_prefix}_{sw}x{sh}_int8.onnx'
                try:
                    subprocess.check_call(
                        f"""
                            python -m bonseyes_ aiassetgaze.optimize.post_training_quantization.onnx_quantize \
                            --input-model {onnx_model_path} \
                            --output-model {model_out_path} \
                            --calibrate-dataset {args.calibrate_dataset} \
                            --per-channel False
                        """,
                        shell=True,
                    )
                except subprocess.CalledProcessError as e:
                    sys.exit(e.returncode)
                os.remove(f'{onnx_models_root}/{model.split("_", 1)[0].split(".")[0]}.onnx')

            if 'tensorrt' in engines:
                engine_save_path = f'{tensorrt_models_root}/{model_prefix}_{sw}x{sh}_int8'
                dla_enabled = args.enable_dla
                if dla_enabled:
                    engine_save_path += '_dla_enabled'
                else:
                    engine_save_path += '_dla_disabled'
                engine_save_path += '.trt'
                enable_dla = '--enable-dla' if args.enable_dla else ''

                try:
                    subprocess.check_call(
                        f"""
                            python -m bonseyes_ aiassetgaze.optimize.post_training_quantization.trt_quantize \
                            --onnx-model {onnx_model_path} \
                            --output-dir {tensorrt_models_root} \
                            --calibrate-dataset {args.calibrate_dataset} \
                            --workspace-size {args.workspace_size} \
                            --workspace-unit {args.workspace_unit} \
                            {enable_dla}
                        """,
                        shell=True,
                    )
                except subprocess.CalledProcessError as e:
                    sys.exit(e.returncode)


if __name__ == '__main__':
    main()
