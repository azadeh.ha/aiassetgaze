import renderer_bind as rb

import cv2


image = cv2.imread('')
rendered_image = rb.render(image)
cv2.imshow('test rendering', rendered_image)