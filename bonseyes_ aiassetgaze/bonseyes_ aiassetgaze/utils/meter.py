import multiprocessing
import subprocess
import platform
import psutil
import sys
import os

import time
import json


class HardwareStatusMeter(multiprocessing.Process):
    def __init__(self, sample_rate_sec=0.5):
        super().__init__()

        self.sample_rate_sec = sample_rate_sec

        self.cpu_only = False
        self.gpu_name = ''
        self.running_on = self._detect_environment()

        if self.running_on == 'x86_64':
            try:
                import nvidia_smi
                nvidia_smi.nvmlInit()

                self.x86_64_has_gpu = True
                self.x86_64_cuda_gpu_handle = nvidia_smi.nvmlDeviceGetHandleByIndex(0)
                gpu_name_raw = nvidia_smi.nvmlDeviceGetName(self.x86_64_cuda_gpu_handle).decode()
                self.gpu_name = gpu_name_raw.replace(' ', '_')
            except Exception as e:
                self.x86_64_has_gpu = False
                self.cpu_only = True

        elif self.running_on == 'jetson':
            from jtop import jtop, JtopException
            self.gpu_name = '512-core_Volta_GPU'
            self.jetson = jtop()
            self.jetson.attach(self.get_all_stats_jetson)

        elif self.running_on == 'raspberrypi':
            self.cpu_only = True

        self.result_detailed = multiprocessing.Manager().dict()
        self.result_summary = multiprocessing.Manager().dict()
        self.counter = 1

        # CPU metrics
        self.cpu_load_acc = 0
        self.cpu_memory_acc = 0
        self.cpu_current_memory_gb_acc = 0
        self.cpu_temp_acc = 0

        self.cpu_load = multiprocessing.Manager().dict({'avg': 0, 'min': 1000, 'max': 0, 'current': 0})
        self.cpu_memory = multiprocessing.Manager().dict({'avg': 0, 'min': 1000, 'max': 0})
        self.cpu_current_memory_gb = multiprocessing.Manager().dict({'avg': 0, 'min': 1000, 'max': 0})
        self.cpu_temp = multiprocessing.Manager().dict({'avg': 0, 'min': 1000, 'max': 0})

        # GPU metrics
        self.gpu_load_acc = 0
        self.gpu_memory_acc = 0
        self.gpu_current_memory_gb_acc = 0
        self.gpu_temperature_acc = 0

        self.gpu_load = multiprocessing.Manager().dict({'avg': 0, 'min': 1000, 'max': 0, 'current': 0})
        self.gpu_memory = multiprocessing.Manager().dict({'avg': 0, 'min': 1000, 'max': 0})
        self.gpu_current_memory_gb = multiprocessing.Manager().dict({'avg': 0, 'min': 1000, 'max': 0, 'current': 0})
        self.gpu_temperature = multiprocessing.Manager().dict({'avg': 0, 'min': 1000, 'max': 0})

        # NPU metrics
        self.npu_load_acc = 0
        self.npu_load = multiprocessing.Manager().dict({'avg': 0, 'min': 1000, 'max': 0})

        # General metrics
        self.memory_bw_acc = 0
        self.power_acc = 0

        self.memory_bw = multiprocessing.Manager().dict({'avg': 0, 'min': 1000, 'max': 0})
        self.power = multiprocessing.Manager().dict({'avg': 0, 'min': 1000, 'max': 0})

    def run(self):
        if self.running_on == 'x86_64':
            while True:
                time.sleep(self.sample_rate_sec)

                cpu_stats = self._get_cpu_stats()
                gpu_stats = {}
                if self.x86_64_has_gpu:
                    gpu_stats = self._get_gpu_stats_x86_64_cuda()

                stats = {**cpu_stats, **gpu_stats}
                self._calculate_stats(stats)
                self.counter += 1

        if self.running_on == 'raspberrypi':
            while True:
                time.sleep(self.sample_rate_sec)

                cpu_stats = self._get_cpu_stats()
                stats = cpu_stats
                self._calculate_stats(stats)
                self.counter += 1

        if self.running_on == 'jetson':
            try:
                self.jetson.loop_for_ever()
            except JtopException as e:
                return

    def terminate(self):
        if not self.cpu_only:
            gpu_load_dict = dict(self.gpu_load)
            gpu_memory_dict = dict(self.gpu_memory)
            gpu_current_memory_gb_dict = dict(self.gpu_current_memory_gb)
            gpu_temperature_dict = dict(self.gpu_temperature)
        else:
            gpu_load_dict = '-'
            gpu_memory_dict = '-'
            gpu_current_memory_gb_dict = '-'
            gpu_temperature_dict = '-'

        self.result_detailed = dict({
            'cpu_load': dict(self.cpu_load),
            'cpu_memory': dict(self.cpu_memory),
            'cpu_current_memory_gb': dict(self.cpu_current_memory_gb),
            'cpu_temperature': dict(self.cpu_temp),
            'gpu_load': gpu_load_dict,
            'gpu_memory': gpu_memory_dict,
            'gpu_current_memory_gb': gpu_current_memory_gb_dict,
            'gpu_temperature': gpu_temperature_dict,
            'memory_bw': dict(self.memory_bw),
            'power': dict(self.power)
        })
        self.result_summary = dict({
            'cpu_load (%)': self.cpu_load['avg'],
            'cpu_memory (%)': self.cpu_memory['avg'],
            'cpu_memory_peak (GB)': self.cpu_current_memory_gb['max'],
            'cpu_temperature (C)': self.cpu_temp['avg'],
            'gpu_load (%)': self.gpu_load['avg'],
            'gpu_memory (%)': self.gpu_memory['avg'],
            'gpu_memory_peak (GB)': self.gpu_current_memory_gb['max'],
            'gpu_temperature (C)': self.gpu_temperature['avg'],
            'memory_bw (%)': self.memory_bw['avg'],
            'memory_bw_peak (%)': self.memory_bw['max'],
            'power (W)': self.power['avg']
        })
        if self.running_on == 'jetson':
            self.result_detailed['npu_load'] = dict(self.npu_load)
            self.result_summary['npu_load'] = self.npu_load['avg']

        super().terminate()

    def collect(self):
        self.terminate()
        self.join()

        return self.result_detailed, self.result_summary

    def summary_to_csv(self, path):
        # CPU metrics
        cpu_load = self.cpu_load['avg']
        cpu_memory = self.cpu_memory['avg']
        cpu_memory_peak = self.cpu_current_memory_gb['max']
        cpu_temperature = self.cpu_temp['avg']
        # GPU metrics
        gpu_load = self.gpu_load['avg']
        gpu_memory = self.gpu_memory['avg']
        gpu_memory_peak = self.gpu_current_memory_gb['max']
        gpu_temperature = self.gpu_temperature['avg']
        # System metrics
        memory_bw = self.memory_bw['avg']
        memory_bw_peak = self.memory_bw['max']
        power = self.power['avg']

        csv_header = \
            'CPU_LOAD,CPU_MEMORY,CPU_MEMORY_PEAK,CPU_TEMPERATURE,GPU_LOAD,GPU_MEMORY,GPU_MEMORY_PEAK,GPU_TEMPERATURE'
        csv_row = \
            f'{cpu_load},{cpu_memory},{cpu_memory_peak},{cpu_temperature},{gpu_load},{gpu_memory},{gpu_memory_peak},{gpu_temperature}'

        # NPU metrics
        if self.running_on == 'jetson':
            npu_load = self.npu_load['avg']
            csv_header += ',NPU_LOAD'
            csv_row += f',{npu_load}'

        # Add system metrics at the end
        csv_header += ',MEMORY_BW,MEMORY_BW_PEAK,POWER'
        csv_row += f',{memory_bw},{memory_bw_peak},{power}'

        with open(path, 'w') as csv_file:
            csv_file.write(csv_header + '\n')
            csv_file.write(csv_row + '\n')

    def _detect_environment(self):
        detected_env = ''
        if platform.machine() == 'x86_64':
            detected_env = 'x86_64'

        if platform.machine() == 'aarch64':
            if 'tegra' in os.uname().release:
                detected_env = 'jetson'
            if 'raspi' in os.uname().release:
                detected_env = 'raspberrypi'

        return detected_env

    def _calculate_stats(self, stats):
        # CPU metrics
        self.cpu_load['current'] = stats['cpu_load']
        if stats['cpu_load'] > self.cpu_load['max']:
            self.cpu_load['max'] = stats['cpu_load']
        if stats['cpu_load'] < self.cpu_load['min']:
            self.cpu_load['min'] = stats['cpu_load']
        self.cpu_load_acc += stats['cpu_load']
        self.cpu_load['avg'] = self.cpu_load_acc / self.counter

        if stats['cpu_memory'] > self.cpu_memory['max']:
            self.cpu_memory['max'] = stats['cpu_memory']
        if stats['cpu_memory'] < self.cpu_memory['min']:
            self.cpu_memory['min'] = stats['cpu_memory']
        self.cpu_memory_acc += stats['cpu_memory']
        self.cpu_memory['avg'] = self.cpu_memory_acc / self.counter

        if stats['cpu_current_memory_gb'] > self.cpu_current_memory_gb['max']:
            self.cpu_current_memory_gb['max'] = stats['cpu_current_memory_gb']
        if stats['cpu_current_memory_gb'] < self.cpu_current_memory_gb['min']:
            self.cpu_current_memory_gb['min'] = stats['cpu_current_memory_gb']
        self.cpu_current_memory_gb_acc += stats['cpu_current_memory_gb']
        self.cpu_current_memory_gb['avg'] = self.cpu_current_memory_gb_acc / self.counter

        if stats['cpu_temperature'] > self.cpu_temp['max']:
            self.cpu_temp['max'] = stats['cpu_temperature']
        if stats['cpu_temperature'] < self.cpu_temp['min']:
            self.cpu_temp['min'] = stats['cpu_temperature']
        self.cpu_temp_acc += stats['cpu_temperature']
        self.cpu_temp['avg'] = self.cpu_temp_acc / self.counter

        # GPU metrics
        if not self.cpu_only:
            self.gpu_load['current'] = stats['gpu_load']
            if stats['gpu_load'] > self.gpu_load['max']:
                self.gpu_load['max'] = stats['gpu_load']
            if stats['gpu_load'] < self.gpu_load['min']:
                self.gpu_load['min'] = stats['gpu_load']
            self.gpu_load_acc += stats['gpu_load']
            self.gpu_load['avg'] = self.gpu_load_acc / self.counter

            if stats['gpu_memory'] > self.gpu_memory['max']:
                self.gpu_memory['max'] = stats['gpu_memory']
            if stats['gpu_memory'] < self.gpu_memory['min']:
                self.gpu_memory['min'] = stats['gpu_memory']
            self.gpu_memory_acc += stats['gpu_memory']
            self.gpu_memory['avg'] = self.gpu_memory_acc / self.counter

            self.gpu_current_memory_gb['current'] = stats['gpu_current_memory_gb']
            if stats['gpu_current_memory_gb'] > self.gpu_current_memory_gb['max']:
                self.gpu_current_memory_gb['max'] = stats['gpu_current_memory_gb']
            if stats['gpu_current_memory_gb'] < self.gpu_current_memory_gb['min']:
                self.gpu_current_memory_gb['min'] = stats['gpu_current_memory_gb']
            self.gpu_current_memory_gb_acc += stats['gpu_current_memory_gb']
            self.gpu_current_memory_gb['avg'] = self.gpu_current_memory_gb_acc / self.counter

            if stats['gpu_temperature'] > self.gpu_temperature['max']:
                self.gpu_temperature['max'] = stats['gpu_temperature']
            if stats['gpu_temperature'] < self.gpu_temperature['min']:
                self.gpu_temperature['min'] = stats['gpu_temperature']
            self.gpu_temperature_acc += stats['gpu_temperature']
            self.gpu_temperature['avg'] = self.gpu_temperature_acc / self.counter

            # NPU metrics
            if self.running_on == 'jetson':
                if stats['npu_load'] > self.npu_load['max']:
                    self.npu_load['max'] = stats['npu_load']
                if stats['npu_load'] < self.npu_load['min']:
                    self.npu_load['min'] = stats['npu_load']
                self.npu_load_acc += stats['npu_load']
                self.npu_load['avg'] = self.npu_load_acc / self.counter

                # System metrics
                if stats['memory_bw'] > self.memory_bw['max']:
                    self.memory_bw['max'] = stats['memory_bw']
                if stats['memory_bw'] < self.memory_bw['min']:
                    self.memory_bw['min'] = stats['memory_bw']
                self.memory_bw_acc += stats['memory_bw']
                self.memory_bw['avg'] = self.memory_bw_acc / self.counter

                if stats['power_current'] > self.power['max']:
                    self.power['max'] = stats['power_current']
                if stats['power_current'] < self.power['min']:
                    self.power['min'] = stats['power_current']
                self.power_acc += stats['power_current']
                self.power['avg'] = self.power_acc / self.counter

    def _get_cpu_stats(self):
        cpu_load = psutil.cpu_percent()
        cpu_memory = psutil.virtual_memory().percent
        cpu_current_memory_gb = psutil.virtual_memory().used / 1e9

        temp_raw = subprocess.getoutput("cat /sys/class/thermal/thermal_zone*/temp")

        # TODO improvement - get temperature as average of all cores if core_num > 1
        # Temp for multiple cores - get first
        if '\n' in temp_raw:
            temp_raw = temp_raw.split('\n')[0]

        cpu_temperature = int(temp_raw) / 1000
        return {
            'cpu_load': cpu_load,
            'cpu_memory': cpu_memory,
            'cpu_current_memory_gb': cpu_current_memory_gb,
            'cpu_temperature': cpu_temperature
        }

    def _get_gpu_stats_x86_64_cuda(self):
        # TODO improvement - move to init somehow (solve multiprocessing nvidia issue)
        import nvidia_smi
        nvidia_smi.nvmlInit()
        self.x86_64_cuda_gpu_handle = nvidia_smi.nvmlDeviceGetHandleByIndex(0)

        gpu_load = nvidia_smi.nvmlDeviceGetUtilizationRates(self.x86_64_cuda_gpu_handle).gpu
        gpu_temperature = nvidia_smi.nvmlDeviceGetTemperature(self.x86_64_cuda_gpu_handle, 0)
        gpu_memory = nvidia_smi.nvmlDeviceGetUtilizationRates(self.x86_64_cuda_gpu_handle).memory
        gpu_current_memory_gb = nvidia_smi.nvmlDeviceGetMemoryInfo(self.x86_64_cuda_gpu_handle).used / 1e9
        return {
            'gpu_load': gpu_load,
            'gpu_memory': gpu_memory,
            'gpu_current_memory_gb': gpu_current_memory_gb,
            'gpu_temperature': gpu_temperature
        }

    def _get_gpu_stats_jetson(self, jetson):
        tegra_stats = jetson.stats

        gpu_load = tegra_stats['GPU']
        gpu_current_memory_gb = tegra_stats['RAM'] / 1e6
        gpu_temeprature = tegra_stats['Temp GPU']
        gpu_memory = gpu_current_memory_gb / (jetson.ram['tot'] / 1e6) * 100  # percentage

        # Check NPU availability
        npu_load = 0
        path = '/sys/devices/platform/host1x/15880000.nvdla0/power/runtime_usage'
        if os.path.isfile(path):
            npu_load_raw = subprocess.getoutput(f'cat {path}')
            npu_load = float(npu_load_raw)

        return {
            'gpu_load': gpu_load,
            'gpu_memory': gpu_memory,
            'gpu_current_memory_gb': gpu_current_memory_gb,
            'gpu_temperature': gpu_temeprature,
            'npu_load': npu_load
        }

    def _get_sys_stats_jetson(self, jetson):
        emc_dict = jetson.emc
        power_dict = jetson.power

        memory_bw = emc_dict['val']
        power_current = power_dict[0]['cur'] / 1e3

        return {
            'memory_bw': memory_bw,
            'power_current': power_current
        }

    def get_all_stats_jetson(self, jetson):
        cpu_stats = self._get_cpu_stats()
        gpu_stats = self._get_gpu_stats_jetson(jetson)
        sys_stats = self._get_sys_stats_jetson(jetson)

        stats = {**cpu_stats, **gpu_stats, **sys_stats}
        try:
            self._calculate_stats(stats)
        except Exception as e:
            sys.exit(0)
        self.counter += 1

        return stats


if __name__ == '__main__':
    """ Start meter """
    m = HardwareStatusMeter()
    m.start()

    """ Replace while loop with your code (evaluation) """
    print('Enter q to terminate program.')
    while True:
        user_input = input()
        if user_input == 'q':
            break

    """ Terminate meter and collect result """
    detailed_stats, summary_stats = m.collect()

    """ Print results """
    print(json.dumps(detailed_stats, indent=2))
    print(json.dumps(summary_stats, indent=2))

    """ Store results as json or csv """
    with open(f'bonseyes_meter_results_detailed.json', 'w') as result_json_detailed:
        json.dump(detailed_stats, result_json_detailed, indent=2)
    with open(f'bonseyes_meter_results_summary.json', 'w') as result_json_summary:
        json.dump(summary_stats, result_json_summary, indent=2)

    m.summary_to_csv('bonseyes_meter_results_summary.csv')

