import requests
import argparse
import json
import sys
import os


def cli():
    parser = argparse.ArgumentParser(description='Bonseyes AI Asset http client')
    parser.add_argument('--input', '-i', required=True, type=str,
                        help='Path to input image or directory with input images')
    parser.add_argument('--port', '-p', required=True, type=str, help='Port name')
    parser.add_argument('--json-output', '-jso', required=True, type=str,
                        help='Path to directory for storing json outputs')
    parser.add_argument('--server-ip', '-si', required=True, type=str, help='Server IP address')

    return parser


def main():
    args = cli().parse_args()

    headers = {}
    srv_addr = 'http://' + args.server_ip + ':' + args.port

    inputs_ = []
    if not os.path.isdir(args.input):
        inputs_.append(args.input)
    else:
        for r, d, f in os.walk(args.input):
            for file in f:
                inputs_.append(os.path.join(r, file))

    for file_path in inputs_:
        file_name = file_path.split('/')[-1]
        input_ = os.path.abspath(file_path)

        output_json = input_.replace(args.input, args.json_output).split('/')
        output_json = '/'.join(output_json[:-1]) + '/%s' % file_name.replace('.jpg', '.json')
        os.makedirs(os.path.dirname(output_json), exist_ok=True)

        with open(input_, 'rb') as fp:
            ret = requests.post(srv_addr + '/inference', data=fp.read(), headers=headers)

        if ret.status_code != 200:
            print("Failed to communicate with worker (error %d)" % ret.status_code, file=sys.stderr)
            print(ret.text, file=sys.stderr)
            sys.exit(1)

        result = ret.json()
        ret.close()

        with open(output_json, 'w') as json_result:
            json.dump(result['prediction'], json_result, indent=2)


if __name__ == '__main__':
    main()
