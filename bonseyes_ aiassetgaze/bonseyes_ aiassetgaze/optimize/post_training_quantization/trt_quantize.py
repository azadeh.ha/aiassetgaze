import argparse
import numpy
import os
import tensorrt
import pycuda.driver as cuda
import pycuda.autoinit

from bonseyes_ aiassetgaze.export.onnx2trt import build_tensorrt_engine, MB, GB
from bonseyes_ aiassetgaze.optimize.post_training_quantization.calibration_data import calibration_dataloader

try:
    EXPLICIT_BATCH = 1 << (int)(tensorrt.NetworkDefinitionCreationFlag.EXPLICIT_BATCH)
except Exception as e:
    # Will fail when mocking imports during doc generation
    pass


class INT8Calibrator(tensorrt.IInt8EntropyCalibrator2):
    def __init__(self, data, cache_file, batch_size=1):
        """
        :param data: numpy array with shape (N, C, H, W)
        :param cache_file:
        :param batch_size:
        """
        tensorrt.IInt8EntropyCalibrator2.__init__(self)

        self._cache_file = cache_file
        self._batch_size = batch_size

        """
        data is numpy array in float32, caution: each image should be normalized
        """
        assert data.ndim == 4 and data.dtype == numpy.float32
        self._data = numpy.array(data, dtype=numpy.float32, order='C')

        self._current_index = 0

        # Allocate enough memory for a whole batch.
        self._device_input = cuda.mem_alloc(self._data[0].nbytes * self._batch_size)

    def get_batch_size(self):
        return self._batch_size

    def get_batch(self, names, p_str=None):
        if self._current_index + self._batch_size > self._data.shape[0]:
            return None

        current_batch = int(self._current_index / self._batch_size)
        if current_batch % 10 == 0:
            print("Calibrating batch {:}, containing {:} images".format(current_batch, self._batch_size))

        batch = self._data[self._current_index : self._current_index + self._batch_size]
        cuda.memcpy_htod(self._device_input, batch)
        self._current_index += self._batch_size
        return [self._device_input]

    def read_calibration_cache(self):
        # If there is a cache, use it instead of calibrating again. Otherwise, implicitly return None.
        if os.path.exists(self._cache_file):
            with open(self._cache_file, "rb") as f:
                return f.read()

    def write_calibration_cache(self, cache):
        with open(self._cache_file, "wb") as f:
            f.write(cache)


def cli():
    parser = argparse.ArgumentParser(description='Bonseyes AI Asset static quantization with TRT tool.')
    parser.add_argument('--onnx-model', '-mo', required=True, type=str, help='Path to model file')
    parser.add_argument('--output-dir', '-od', required=True, type=str, help='Path to converted model output directory')
    parser.add_argument(
        '--workspace-unit',
        '-wu',
        nargs='?',
        const='GB',
        default='GB',
        choices=['MB', 'GB'],
        help='Available units: MB | GB',
    )
    parser.add_argument(
        '--workspace-size',
        '-ws',
        required=False,
        default=4,
        type=int,
        help='Conversion workspace size in GB',
    )
    parser.add_argument(
        '--enable-dla', '-ed', required=False, default=False, action='store_true', help='Enable dla for tensorrt model'
    )
    parser.add_argument(
        "--calibrate-dataset",
        default="/app/bonseyes_ aiassetgaze/<calibration_data_path>",
        help="Calibration data set"
    )
    return parser


def main():
    args = cli().parse_args()
    model_name = args.onnx_model.split('/')[-1].rsplit('_', 1)[0]
    model_name_trt = f'{model_name}_int8'
    if args.enable_dla:
        model_name_trt += '_dla_enabled'
    else:
        model_name_trt += '_dla_disabled'

    model_name_trt += '.trt'
    engine_save_path = f'{args.output_dir}/{model_name_trt}'
    workspace_size = MB(args.workspace_size) if args.workspace_unit == 'MB' else GB(args.workspace_size)

    # Get model input sizes from onnx model name
    # Need size info for int8 calibration
    # model_name = onnx_file_path.split('/')[-1].replace('.onnx', '')
    print(f'model name: {model_name}')
    size_string = model_name.rsplit('_', 1)[-1]
    print(f'size string: {size_string}')
    input_width = int(size_string.split('x')[0])
    input_height = int(size_string.split('x')[1])
    size = (input_width, input_height)

    calibration_data = calibration_dataloader(
        size=size,
        images_num=300,
        dataset_path=args.calibrate_dataset,
        model_path=args.onnx_model,
        engine='onnxruntime'
    )
    cache_file = 'test_cache.cache'
    int8_calibrator = INT8Calibrator(calibration_data, cache_file)

    build_tensorrt_engine(
        args.onnx_model,
        engine_save_path,
        precision_mode='int8',
        max_workspace_size=workspace_size,
        max_batch_size=1,
        min_timing_iterations=2,
        avg_timing_iterations=2,
        dla_enabled=args.enable_dla,
        int8_calibrator=int8_calibrator
    )


if __name__ == '__main__':
    main()
