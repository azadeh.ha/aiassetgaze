#define CATCH_CONFIG_MAIN

#include <iostream>
#include <opencv2/opencv.hpp>

#include "../../deps/catch2/catch.hpp"
#include "../../deps/jsonl/json.hpp"

#include "../include/preprocessor.hpp"


using namespace std;


SCENARIO("<AiAssetName> preprocessing test on <AiAssetDataset> eval subset", "" ) {
    GIVEN("ground truth preprocessed images in json format") {
        string ground_truth_data_root = "";

        WHEN("preprocessing is applied on list of images") {
            THEN("produced results match ground truths") {
                REQUIRE(true == true);
            }
        }
    }
}