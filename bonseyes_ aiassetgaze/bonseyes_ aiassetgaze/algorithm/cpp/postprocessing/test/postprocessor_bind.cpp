#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include <vector>

#include "../include/ aiassetgaze_postprocessor.hpp"

using namespace std;
namespace py = pybind11;


PYBIND11_PLUGIN( aiassetgaze_postprocessor_bind) {
    py::module m(" aiassetgaze_postprocessor_bind", " aiassetgaze postprocessing bind");

    py::class_<AlgorithmPostprocessor>(m, "AlgorithmPostprocessor")
        .def(py::init<>())
        .def("json_predictions", &AlgorithmPostprocessor::json_predictions);

    return m.ptr();
}
